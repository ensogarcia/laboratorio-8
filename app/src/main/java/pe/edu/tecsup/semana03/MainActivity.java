package pe.edu.tecsup.semana03;

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import pe.edu.tecsup.semana03.Utils.Utils;

public class MainActivity extends AppCompatActivity {


    private EditText editTextName;
    private EditText editTextpassword;

    private ListView List;

    public ArrayList NameList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        NameList = new ArrayList<String>();

        editTextpassword = findViewById(R.id.editTextID);
        editTextName = findViewById(R.id.editTextName);

        List = findViewById(R.id.MainList);
        List.setAdapter(new CustomAdapter());
        SQLiteConnectionHelper connectionHelper = new SQLiteConnectionHelper(this, "bd_users", null, 1);

    }

    public void SavePreference(View view) {
        SQLiteConnectionHelper connectionHelper = new SQLiteConnectionHelper(this, "bd_users", null, 1);
        SQLiteDatabase database = connectionHelper.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put(Utils.COLUMN_NAME, editTextName.getText().toString());
        values.put(Utils.COLUMN_PASSWORD, editTextpassword.getText().toString());

        database.insert(Utils.TABLE_USERS, Utils.COLUMN_PASSWORD, values);

        Toast.makeText(this, "save", Toast.LENGTH_LONG).show();
    }





    public class CustomAdapter extends BaseAdapter{

        @Override
        public int getCount() {
            return 0;
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View view, ViewGroup parent) {
            view = getLayoutInflater().inflate(R.layout.list_save, null);
            TextView Name= view.findViewById(R.id.ImageName);
            Name.setText(NameList.get(position).toString());
            //Image.setImageDrawable(getResources().getDrawable((int)ImageList.get(position)));
            return view;
        }
    }
}
