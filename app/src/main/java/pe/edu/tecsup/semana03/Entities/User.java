package pe.edu.tecsup.semana03.Entities;

public class User {
    private int password;
    private String name;

    public User(int password, String name) {
        this.password = password;
        this.name = name;
    }

    public int getId() {
        return password;
    }

    public void setId(int id) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
