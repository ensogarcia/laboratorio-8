package pe.edu.tecsup.semana03;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.widget.Toast;

import pe.edu.tecsup.semana03.Utils.Utils;

public class SQLiteConnectionHelper extends SQLiteOpenHelper {

    private Context _context;

    public SQLiteConnectionHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);

        _context = context;

        Toast.makeText(context, "SQLiteConntectionHelper@constructor", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(Utils.CREATE_TABLE_USERS);

        Toast.makeText(_context, "SQLiteConntectionHelper@onCreate", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS users");
        onCreate(db);
    }



}
