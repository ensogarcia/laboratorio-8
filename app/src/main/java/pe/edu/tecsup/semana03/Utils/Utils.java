package pe.edu.tecsup.semana03.Utils;

public class Utils {
    static public final String CREATE_TABLE_USERS = "CREATE TABLE users (name TEXT, password TEXT)";


    static public final String TABLE_USERS = "users";
    static public final String COLUMN_NAME = "name";
    static public final String COLUMN_PASSWORD = "password";

}
